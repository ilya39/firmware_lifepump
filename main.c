#include "stm32f10x.h"

uint16_t delay(uint32_t k)
	{
		uint32_t i;
		
		for(i=0;i<k;i++) {}
		return(0);
	}

int main(void)
{
	uint8_t j,t;
	uint8_t diode = 0;
	
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;									//GPIO initialization lower
	GPIOC->CRH |= (GPIO_CRH_MODE8|GPIO_CRH_MODE9);
	GPIOC->CRH &= ~(GPIO_CRH_CNF8|GPIO_CRH_CNF9);
	
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;								//UART initialization lower
	USART1->BRR = 0xd1;
	USART1->CR1 |= (USART_CR1_UE|USART_CR1_TE|USART_CR1_RE);
	RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN|RCC_APB2ENR_AFIOEN);
	GPIOA->CRH &= ~(GPIO_CRH_CNF9);
	GPIOA->CRH |= GPIO_CRH_CNF9_1;
	GPIOA->CRH |= GPIO_CRH_MODE9_0;
	GPIOA->CRH &= ~(GPIO_CRH_CNF10);
	GPIOA->CRH |= GPIO_CRH_CNF10_0;
	GPIOA->CRH &= ~(GPIO_CRH_MODE10);
	
	while(1)
	{
		if (diode)
		{
			for(j=0;j<100;(j++))
			{
				GPIOC->BSRR |= (GPIO_BSRR_BS8|GPIO_BSRR_BS9);
				delay(4000000/j);
				GPIOC->BRR |= (GPIO_BSRR_BS8|GPIO_BSRR_BS9);
				delay(4000000/j);
				if(j>=99)
				{
					for(t=1;t<=40;t++)
					{
						GPIOC->BSRR |= (GPIO_BSRR_BS8|GPIO_BSRR_BS9);
						delay(40000*t);
						GPIOC->BRR |= (GPIO_BSRR_BS8|GPIO_BSRR_BS9);
						delay(40000*t);
					}
				}
			}
		}
		else
		{
			if ((USART1->SR & USART_SR_TXE)|(USART1->SR & USART_SR_TC))
			{
				USART1->DR=0x1234;
			}
		}
	}
}
